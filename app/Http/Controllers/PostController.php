<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use App\Post;
use App\User;
class PostController extends Controller
{
    function index(){

        $posts = Post::all();

        if(count($posts) === 0){
            return response()->json([
                'message' => 'there are not any post yet'
            ], 404);
        }

        return response()->json(['posts' => $posts], 200);
    }

    function show($id){
        $post = Post::find($id);
        if(Post::find($id)){
            return response()->json([$post], 200);
        }

        return Response()->json(['message' => 'posts not found'], 404);
    }

    function destroy($id){
        if (Post::destroy($id)){
            return Response()->json(['message' => 'posts removed'], 200);
        }

        return Response()->json(['message' => 'posts not removed'], 404);
    }

    function store(Request $request){

        $user = JWTAuth::toUser($request->token);
        $post = new Post;
        $post->title = $request->get('title');
        $post->description = $request->get('description');
        $post->user_id = $user->id;

        if($post->save()){
            return Response()->json(['message' => 'posts added'], 201);
        }

        return Response()->json(['message' => 'posts not added'], 200);
    }

    function update(Request $request, $id){

        try {
           $post = Post::find($id);
            if(Post::find($id)) {
                $post->title = $request->get('title');
                $post->description = $request->get('description');
                $post->save();
                return Response()->json(['message' => 'posts updated'], 201);
            }

        } catch (JWTAuthException $e) {
            return response()->json(['post does not exists'], 500);
        }


       return Response()->json(['message' => 'posts was not found'], 404); // Status code here
    }
}
