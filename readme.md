How to clone the code?

git clone git@bitbucket.org:robschuh/restapi-jwt-laravel.git

-> create your .env file (rename the .env.example file) with your database information (db name, username and pwd, if it is necessary change the port etc..)
-> php artisan migrate (run the migrations with the db structure)


there is only one branch "master" , normally I work at least with two branches MASTER and DEV.

Demo REST API with Laravel and JWT
Tested by POSTMAN https://www.getpostman.com/

CRUD operations CREATE, READ, UPDATE AND DELETE

FW used: Laravel 5.4
Auth: JSON Web Token Authentication for Laravel - https://github.com/tymondesigns/jwt-auth

Simple post and user controllers.

->php artisan route:list 
       | GET|HEAD | /                    |      | Closure                                         | web          |
|        | POST     | api/v1/auth/login    |      | App\Http\Controllers\UserController@login       | api          |
|        | POST     | api/v1/auth/register |      | App\Http\Controllers\UserController@register    | api          |
|        | GET|HEAD | api/v1/posts         |      | App\Http\Controllers\PostController@index       | api,jwt.auth |
|        | POST     | api/v1/posts         |      | App\Http\Controllers\PostController@store       | api,jwt.auth |
|        | GET|HEAD | api/v1/posts/{id}    |      | App\Http\Controllers\PostController@show        | api,jwt.auth |
|        | PUT      | api/v1/posts/{id}    |      | App\Http\Controllers\PostController@update      | api,jwt.auth |
|        | DELETE   | api/v1/posts/{id}    |      | App\Http\Controllers\PostController@destroy     | api,jwt.auth |
|        | GET|HEAD | api/v1/user          |      | App\Http\Controllers\UserController@getAuthUser | api,jwt.auth 

Endpoints

Register user endpoint (I only documented this endpoint)

    URL: http://<yourdomain>/api/v1/auth/register
    Example: http://restapi.local/api/v1/auth/register

    endpoint: /api/v1/auth/register

    Method: POST

    Data Params

    title: string
	description: string

    Success Response:

        Code: 201
        Content: { status : true , message: User created successfully, data: {name, email, updated_at, created_at} }

    Error Response:

    <Most endpoints will have many ways they can fail. From unauthorized access, to wrongful parameters etc. All of those should be liste d here. It might seem repetitive, but it helps prevent assumptions from being made where they should be.>
        Code: 401 UNAUTHORIZED
        Content: {"error":"User already exists"}

Login user (it is necessary in order to get the TOKEN you will need for the other endpoints)

    URL: http://<yourdomain>/api/v1/auth/login
    Example: http://restapi.local/api/v1/auth/login
    
    endpoint: /api/v1/auth/login
    
    Method: POST
    
    Data Params

    email: string
	password: string
    
Get user account detals

    URL: http://<yourdomain>//api/v1/posts?token=<token>
    Example: http://restapi.local/api/v1/posts?token=eyJ0eXAiOiJKV1QiLCJhbGciOi
    
    endpoint: /api/v1/posts
    
    METHOD: GET
    
    URL Params
    Required: token=[integer] (without quotes, please check the example above)
    
    Headers:
    token: string
    
    
    Success Response:

    Code: 200
    Content: { "posts" : ["id":<integer>, "title":<string>,  description: <string> ]}
    
    
    Error Response:

    Code: 404 NOT FOUND
    Content: { error : "User does not exist }

Note: the next enpoints need a token:
|        | GET|HEAD | api/v1/posts         |      | App\Http\Controllers\PostController@index       | api,jwt.auth |
|        | POST     | api/v1/posts         |      | App\Http\Controllers\PostController@store       | api,jwt.auth |
|        | GET|HEAD | api/v1/posts/{id}    |      | App\Http\Controllers\PostController@show        | api,jwt.auth |
|        | PUT      | api/v1/posts/{id}    |      | App\Http\Controllers\PostController@update      | api,jwt.auth |
|        | DELETE   | api/v1/posts/{id}    |      | App\Http\Controllers\PostController@destroy     | api,jwt.auth |
|        | GET|HEAD | api/v1/user          |      | App\Http\Controllers\UserController@getAuthUser | api,jwt.auth 

